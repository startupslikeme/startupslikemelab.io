require('es6-promise').polyfill();

/* Helpers */
serialize = function(obj) {
  var str = [];
  for(var p in obj)
    if (obj.hasOwnProperty(p)) {
      str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
    }
  return str.join("&");
};

function status(response) {
  json = response.json();
  if (!response.ok) {
    return json.then(Promise.reject.bind(Promise));
  }
  return json;
}

/* API calls */
patchCandidates = function (canidateID, data, successFn, errorFn) {
  fetch(apiURL + '/candidates/' + canidateID, {
    method: 'PATCH',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data),
  }).then(status)
    .then(successFn)
    .catch(errorFn);
};

postCandidates = function (data, successFn, errorFn) {
  fetch(apiURL + '/candidates', {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data),
  }).then(status)
    .then(successFn)
    .catch(errorFn);
};

listRatings = function (params, successFn, errorFn) {
  fetch(apiURL + '/ratings?' + serialize(params), {
    method: 'GET',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
  }).then(status)
    .then(successFn)
    .catch(errorFn);
};

patchRatings = function (ratingID, data, successFn, errorFn) {
  fetch(apiURL + '/ratings/' + ratingID, {
    method: 'PATCH',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data),
  }).then(status)
    .then(successFn)
    .catch(errorFn);
};

postRatings = function (data, successFn, errorFn) {
  fetch(apiURL + '/ratings', {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data),
  }).then(status)
    .then(successFn)
    .catch(errorFn);
};

listStartups = function (params, successFn, errorFn) {
  fetch(apiURL + '/startups?' + serialize(params), {
    method: 'GET',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
  }).then(status)
    .then(successFn)
    .catch(errorFn);
};
