/* Globals */
apiVersion = 'v1';
apiServer = (NODE_ENV === 'development') ? '//localhost:9292/' : '//api.startupslike.me/';
apiURL = (window.location.protocol + apiServer + apiVersion);

/* Backend */
require('./client.js');

/* Web notifications */
var OneSignal = window.OneSignal || [];
if (NODE_ENV === 'development') {
  var OneSignalAppId = "9b2c1b78-a0d9-4760-92b3-3ef3b7589730";
  var OneSignalsubdomainName = "startupslikemelocal";
} else {
  var OneSignalAppId = "f8243a89-5790-400f-ab90-0cc7fbbedef6";
  var OneSignalsubdomainName = "startupslikeme";
}
