module.exports = function render(locals) {
  if(locals.path === '404.html') {
    return require('./public/404.pug');
  } else if(locals.path === '/') {
    return require('./public/index.pug');
  } else if(locals.path === '/rate/') {
    return require('./public/rate.pug');
  } else if(locals.path === '/email-frequency/') {
    return require('./public/email-frequency.pug');
  }
};
