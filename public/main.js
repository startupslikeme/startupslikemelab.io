(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else {
		var a = factory();
		for(var i in a) (typeof exports === 'object' ? exports : root)[i] = a[i];
	}
})(this, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	__webpack_require__(1);
	module.exports = __webpack_require__(2);


/***/ },
/* 1 */
/***/ function(module, exports) {

	(function(self) {
	  'use strict';

	  if (self.fetch) {
	    return
	  }

	  var support = {
	    searchParams: 'URLSearchParams' in self,
	    iterable: 'Symbol' in self && 'iterator' in Symbol,
	    blob: 'FileReader' in self && 'Blob' in self && (function() {
	      try {
	        new Blob()
	        return true
	      } catch(e) {
	        return false
	      }
	    })(),
	    formData: 'FormData' in self,
	    arrayBuffer: 'ArrayBuffer' in self
	  }

	  if (support.arrayBuffer) {
	    var viewClasses = [
	      '[object Int8Array]',
	      '[object Uint8Array]',
	      '[object Uint8ClampedArray]',
	      '[object Int16Array]',
	      '[object Uint16Array]',
	      '[object Int32Array]',
	      '[object Uint32Array]',
	      '[object Float32Array]',
	      '[object Float64Array]'
	    ]

	    var isDataView = function(obj) {
	      return obj && DataView.prototype.isPrototypeOf(obj)
	    }

	    var isArrayBufferView = ArrayBuffer.isView || function(obj) {
	      return obj && viewClasses.indexOf(Object.prototype.toString.call(obj)) > -1
	    }
	  }

	  function normalizeName(name) {
	    if (typeof name !== 'string') {
	      name = String(name)
	    }
	    if (/[^a-z0-9\-#$%&'*+.\^_`|~]/i.test(name)) {
	      throw new TypeError('Invalid character in header field name')
	    }
	    return name.toLowerCase()
	  }

	  function normalizeValue(value) {
	    if (typeof value !== 'string') {
	      value = String(value)
	    }
	    return value
	  }

	  // Build a destructive iterator for the value list
	  function iteratorFor(items) {
	    var iterator = {
	      next: function() {
	        var value = items.shift()
	        return {done: value === undefined, value: value}
	      }
	    }

	    if (support.iterable) {
	      iterator[Symbol.iterator] = function() {
	        return iterator
	      }
	    }

	    return iterator
	  }

	  function Headers(headers) {
	    this.map = {}

	    if (headers instanceof Headers) {
	      headers.forEach(function(value, name) {
	        this.append(name, value)
	      }, this)

	    } else if (headers) {
	      Object.getOwnPropertyNames(headers).forEach(function(name) {
	        this.append(name, headers[name])
	      }, this)
	    }
	  }

	  Headers.prototype.append = function(name, value) {
	    name = normalizeName(name)
	    value = normalizeValue(value)
	    var oldValue = this.map[name]
	    this.map[name] = oldValue ? oldValue+','+value : value
	  }

	  Headers.prototype['delete'] = function(name) {
	    delete this.map[normalizeName(name)]
	  }

	  Headers.prototype.get = function(name) {
	    name = normalizeName(name)
	    return this.has(name) ? this.map[name] : null
	  }

	  Headers.prototype.has = function(name) {
	    return this.map.hasOwnProperty(normalizeName(name))
	  }

	  Headers.prototype.set = function(name, value) {
	    this.map[normalizeName(name)] = normalizeValue(value)
	  }

	  Headers.prototype.forEach = function(callback, thisArg) {
	    for (var name in this.map) {
	      if (this.map.hasOwnProperty(name)) {
	        callback.call(thisArg, this.map[name], name, this)
	      }
	    }
	  }

	  Headers.prototype.keys = function() {
	    var items = []
	    this.forEach(function(value, name) { items.push(name) })
	    return iteratorFor(items)
	  }

	  Headers.prototype.values = function() {
	    var items = []
	    this.forEach(function(value) { items.push(value) })
	    return iteratorFor(items)
	  }

	  Headers.prototype.entries = function() {
	    var items = []
	    this.forEach(function(value, name) { items.push([name, value]) })
	    return iteratorFor(items)
	  }

	  if (support.iterable) {
	    Headers.prototype[Symbol.iterator] = Headers.prototype.entries
	  }

	  function consumed(body) {
	    if (body.bodyUsed) {
	      return Promise.reject(new TypeError('Already read'))
	    }
	    body.bodyUsed = true
	  }

	  function fileReaderReady(reader) {
	    return new Promise(function(resolve, reject) {
	      reader.onload = function() {
	        resolve(reader.result)
	      }
	      reader.onerror = function() {
	        reject(reader.error)
	      }
	    })
	  }

	  function readBlobAsArrayBuffer(blob) {
	    var reader = new FileReader()
	    var promise = fileReaderReady(reader)
	    reader.readAsArrayBuffer(blob)
	    return promise
	  }

	  function readBlobAsText(blob) {
	    var reader = new FileReader()
	    var promise = fileReaderReady(reader)
	    reader.readAsText(blob)
	    return promise
	  }

	  function readArrayBufferAsText(buf) {
	    var view = new Uint8Array(buf)
	    var chars = new Array(view.length)

	    for (var i = 0; i < view.length; i++) {
	      chars[i] = String.fromCharCode(view[i])
	    }
	    return chars.join('')
	  }

	  function bufferClone(buf) {
	    if (buf.slice) {
	      return buf.slice(0)
	    } else {
	      var view = new Uint8Array(buf.byteLength)
	      view.set(new Uint8Array(buf))
	      return view.buffer
	    }
	  }

	  function Body() {
	    this.bodyUsed = false

	    this._initBody = function(body) {
	      this._bodyInit = body
	      if (!body) {
	        this._bodyText = ''
	      } else if (typeof body === 'string') {
	        this._bodyText = body
	      } else if (support.blob && Blob.prototype.isPrototypeOf(body)) {
	        this._bodyBlob = body
	      } else if (support.formData && FormData.prototype.isPrototypeOf(body)) {
	        this._bodyFormData = body
	      } else if (support.searchParams && URLSearchParams.prototype.isPrototypeOf(body)) {
	        this._bodyText = body.toString()
	      } else if (support.arrayBuffer && support.blob && isDataView(body)) {
	        this._bodyArrayBuffer = bufferClone(body.buffer)
	        // IE 10-11 can't handle a DataView body.
	        this._bodyInit = new Blob([this._bodyArrayBuffer])
	      } else if (support.arrayBuffer && (ArrayBuffer.prototype.isPrototypeOf(body) || isArrayBufferView(body))) {
	        this._bodyArrayBuffer = bufferClone(body)
	      } else {
	        throw new Error('unsupported BodyInit type')
	      }

	      if (!this.headers.get('content-type')) {
	        if (typeof body === 'string') {
	          this.headers.set('content-type', 'text/plain;charset=UTF-8')
	        } else if (this._bodyBlob && this._bodyBlob.type) {
	          this.headers.set('content-type', this._bodyBlob.type)
	        } else if (support.searchParams && URLSearchParams.prototype.isPrototypeOf(body)) {
	          this.headers.set('content-type', 'application/x-www-form-urlencoded;charset=UTF-8')
	        }
	      }
	    }

	    if (support.blob) {
	      this.blob = function() {
	        var rejected = consumed(this)
	        if (rejected) {
	          return rejected
	        }

	        if (this._bodyBlob) {
	          return Promise.resolve(this._bodyBlob)
	        } else if (this._bodyArrayBuffer) {
	          return Promise.resolve(new Blob([this._bodyArrayBuffer]))
	        } else if (this._bodyFormData) {
	          throw new Error('could not read FormData body as blob')
	        } else {
	          return Promise.resolve(new Blob([this._bodyText]))
	        }
	      }

	      this.arrayBuffer = function() {
	        if (this._bodyArrayBuffer) {
	          return consumed(this) || Promise.resolve(this._bodyArrayBuffer)
	        } else {
	          return this.blob().then(readBlobAsArrayBuffer)
	        }
	      }
	    }

	    this.text = function() {
	      var rejected = consumed(this)
	      if (rejected) {
	        return rejected
	      }

	      if (this._bodyBlob) {
	        return readBlobAsText(this._bodyBlob)
	      } else if (this._bodyArrayBuffer) {
	        return Promise.resolve(readArrayBufferAsText(this._bodyArrayBuffer))
	      } else if (this._bodyFormData) {
	        throw new Error('could not read FormData body as text')
	      } else {
	        return Promise.resolve(this._bodyText)
	      }
	    }

	    if (support.formData) {
	      this.formData = function() {
	        return this.text().then(decode)
	      }
	    }

	    this.json = function() {
	      return this.text().then(JSON.parse)
	    }

	    return this
	  }

	  // HTTP methods whose capitalization should be normalized
	  var methods = ['DELETE', 'GET', 'HEAD', 'OPTIONS', 'POST', 'PUT']

	  function normalizeMethod(method) {
	    var upcased = method.toUpperCase()
	    return (methods.indexOf(upcased) > -1) ? upcased : method
	  }

	  function Request(input, options) {
	    options = options || {}
	    var body = options.body

	    if (typeof input === 'string') {
	      this.url = input
	    } else {
	      if (input.bodyUsed) {
	        throw new TypeError('Already read')
	      }
	      this.url = input.url
	      this.credentials = input.credentials
	      if (!options.headers) {
	        this.headers = new Headers(input.headers)
	      }
	      this.method = input.method
	      this.mode = input.mode
	      if (!body && input._bodyInit != null) {
	        body = input._bodyInit
	        input.bodyUsed = true
	      }
	    }

	    this.credentials = options.credentials || this.credentials || 'omit'
	    if (options.headers || !this.headers) {
	      this.headers = new Headers(options.headers)
	    }
	    this.method = normalizeMethod(options.method || this.method || 'GET')
	    this.mode = options.mode || this.mode || null
	    this.referrer = null

	    if ((this.method === 'GET' || this.method === 'HEAD') && body) {
	      throw new TypeError('Body not allowed for GET or HEAD requests')
	    }
	    this._initBody(body)
	  }

	  Request.prototype.clone = function() {
	    return new Request(this, { body: this._bodyInit })
	  }

	  function decode(body) {
	    var form = new FormData()
	    body.trim().split('&').forEach(function(bytes) {
	      if (bytes) {
	        var split = bytes.split('=')
	        var name = split.shift().replace(/\+/g, ' ')
	        var value = split.join('=').replace(/\+/g, ' ')
	        form.append(decodeURIComponent(name), decodeURIComponent(value))
	      }
	    })
	    return form
	  }

	  function parseHeaders(rawHeaders) {
	    var headers = new Headers()
	    rawHeaders.split('\r\n').forEach(function(line) {
	      var parts = line.split(':')
	      var key = parts.shift().trim()
	      if (key) {
	        var value = parts.join(':').trim()
	        headers.append(key, value)
	      }
	    })
	    return headers
	  }

	  Body.call(Request.prototype)

	  function Response(bodyInit, options) {
	    if (!options) {
	      options = {}
	    }

	    this.type = 'default'
	    this.status = 'status' in options ? options.status : 200
	    this.ok = this.status >= 200 && this.status < 300
	    this.statusText = 'statusText' in options ? options.statusText : 'OK'
	    this.headers = new Headers(options.headers)
	    this.url = options.url || ''
	    this._initBody(bodyInit)
	  }

	  Body.call(Response.prototype)

	  Response.prototype.clone = function() {
	    return new Response(this._bodyInit, {
	      status: this.status,
	      statusText: this.statusText,
	      headers: new Headers(this.headers),
	      url: this.url
	    })
	  }

	  Response.error = function() {
	    var response = new Response(null, {status: 0, statusText: ''})
	    response.type = 'error'
	    return response
	  }

	  var redirectStatuses = [301, 302, 303, 307, 308]

	  Response.redirect = function(url, status) {
	    if (redirectStatuses.indexOf(status) === -1) {
	      throw new RangeError('Invalid status code')
	    }

	    return new Response(null, {status: status, headers: {location: url}})
	  }

	  self.Headers = Headers
	  self.Request = Request
	  self.Response = Response

	  self.fetch = function(input, init) {
	    return new Promise(function(resolve, reject) {
	      var request = new Request(input, init)
	      var xhr = new XMLHttpRequest()

	      xhr.onload = function() {
	        var options = {
	          status: xhr.status,
	          statusText: xhr.statusText,
	          headers: parseHeaders(xhr.getAllResponseHeaders() || '')
	        }
	        options.url = 'responseURL' in xhr ? xhr.responseURL : options.headers.get('X-Request-URL')
	        var body = 'response' in xhr ? xhr.response : xhr.responseText
	        resolve(new Response(body, options))
	      }

	      xhr.onerror = function() {
	        reject(new TypeError('Network request failed'))
	      }

	      xhr.ontimeout = function() {
	        reject(new TypeError('Network request failed'))
	      }

	      xhr.open(request.method, request.url, true)

	      if (request.credentials === 'include') {
	        xhr.withCredentials = true
	      }

	      if ('responseType' in xhr && support.blob) {
	        xhr.responseType = 'blob'
	      }

	      request.headers.forEach(function(value, name) {
	        xhr.setRequestHeader(name, value)
	      })

	      xhr.send(typeof request._bodyInit === 'undefined' ? null : request._bodyInit)
	    })
	  }
	  self.fetch.polyfill = true
	})(typeof self !== 'undefined' ? self : this);


/***/ },
/* 2 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = function render(locals) {
	  if(locals.path === '404.html') {
	    return __webpack_require__(3);
	  } else if(locals.path === '/') {
	    return __webpack_require__(4);
	  } else if(locals.path === '/rate/') {
	    return __webpack_require__(5);
	  } else if(locals.path === '/email-frequency/') {
	    return __webpack_require__(6);
	  }
	};


/***/ },
/* 3 */
/***/ function(module, exports) {

	module.exports = "<!DOCTYPE html><html lang=\"en\"><head><meta charset=\"utf-8\"><meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\"><meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\"><meta name=\"description\" content=\"Get alerts for the best startups for you.\"><title>Page not found | StartupsLikeMe</title><link rel=\"icon\" href=\"/images/favicon.png\" type=\"image/png\"><link rel=\"stylesheet\" href=\"//maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/css/bootstrap.min.css\" integrity=\"sha384-AysaV+vQoT3kOAXZkl02PThvDr8HYKPZhNT5h/CXfBThSRXQ6jW5DO2ekP5ViFdi\" crossorigin=\"anonymous\"><link href=\"//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css\" rel=\"stylesheet\" integrity=\"sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN\" crossorigin=\"anonymous\"><link rel=\"stylesheet\" href=\"/css/style.css\"><script src=\"https://cdn.onesignal.com/sdks/OneSignalSDK.js\" async=\"async\"></script><script type=\"text/javascript\">// development\n/*var OneSignal = window.OneSignal || [];\nOneSignal.push([\"init\", {\n  appId: \"9b2c1b78-a0d9-4760-92b3-3ef3b7589730\",\n  autoRegister: false,\n  subdomainName: 'startupslikemelocal',\n  safari_web_id: 'web.onesignal.auto.3c6dd35c-6a89-4a57-8823-48d881ba6e9f',\n  httpPermissionRequest: {\n    enable: true\n  },\n  notifyButton: {\n      enable: false\n  }\n}]);*/\n// production\nvar OneSignal = window.OneSignal || [];\nOneSignal.push([\"init\", {\n  appId: \"f8243a89-5790-400f-ab90-0cc7fbbedef6\",\n  autoRegister: false,\n  subdomainName: 'startupslikeme',\n  safari_web_id: 'web.onesignal.auto.235723f6-f2ef-49f2-bb5e-e966332d4e54',\n  httpPermissionRequest: {\n    enable: true\n  },\n  notifyButton: {\n      enable: false\n  }\n}]);</script></head><body class=\"align-middle text-xs-center\"><nav class=\"navbar\"><a class=\"navbar-brand\" href=\"/\"><img class=\"align-top\" src=\"/images/logo.png\" height=\"30\" alt=\"Startups Like Me\"></a></nav><div class=\"alert alert-danger\" id=\"globalError\" role=\"alert\" style=\"display:none\">Bad response. Please try again later.</div><section class=\"jumbotron text-xs-center\" style=\"background-color: #dedede\"><h1 class=\"display-3 mb-3\">Page not found</h1><p class=\"lead\"><a class=\"btn btn-primary btn-lg\" href=\"/\" role=\"button\">Take me home</a></p></section><script type=\"text/javascript\">(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){\n(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),\nm=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)\n})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');\n\nga('create', 'UA-88453796-1', 'auto');\nga('send', 'pageview');</script><script src=\"//code.jquery.com/jquery-3.1.1.min.js\" integrity=\"sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=\" crossorigin=\"anonymous\"></script><script src=\"/app.min.js\"></script><script type=\"text/javascript\">$(function() {\n  /* Utils */\n  $.urlParam = function(name){\n    var results = new RegExp('[\\\\?&]' + name + '=([^&#]*)').exec(window.location.href);\n    return results[1] || 0;\n  };\n});</script></body></html>";

/***/ },
/* 4 */
/***/ function(module, exports) {

	module.exports = "<!DOCTYPE html><html lang=\"en\"><head><meta charset=\"utf-8\"><meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\"><meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\"><meta name=\"description\" content=\"Get alerts for the best startups for you.\"><title>Best startups for you | StartupsLikeMe</title><link rel=\"icon\" href=\"/images/favicon.png\" type=\"image/png\"><link rel=\"stylesheet\" href=\"//maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/css/bootstrap.min.css\" integrity=\"sha384-AysaV+vQoT3kOAXZkl02PThvDr8HYKPZhNT5h/CXfBThSRXQ6jW5DO2ekP5ViFdi\" crossorigin=\"anonymous\"><link href=\"//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css\" rel=\"stylesheet\" integrity=\"sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN\" crossorigin=\"anonymous\"><link rel=\"stylesheet\" href=\"/css/style.css\"><script src=\"https://cdn.onesignal.com/sdks/OneSignalSDK.js\" async=\"async\"></script><script type=\"text/javascript\">// development\n/*var OneSignal = window.OneSignal || [];\nOneSignal.push([\"init\", {\n  appId: \"9b2c1b78-a0d9-4760-92b3-3ef3b7589730\",\n  autoRegister: false,\n  subdomainName: 'startupslikemelocal',\n  safari_web_id: 'web.onesignal.auto.3c6dd35c-6a89-4a57-8823-48d881ba6e9f',\n  httpPermissionRequest: {\n    enable: true\n  },\n  notifyButton: {\n      enable: false\n  }\n}]);*/\n// production\nvar OneSignal = window.OneSignal || [];\nOneSignal.push([\"init\", {\n  appId: \"f8243a89-5790-400f-ab90-0cc7fbbedef6\",\n  autoRegister: false,\n  subdomainName: 'startupslikeme',\n  safari_web_id: 'web.onesignal.auto.235723f6-f2ef-49f2-bb5e-e966332d4e54',\n  httpPermissionRequest: {\n    enable: true\n  },\n  notifyButton: {\n      enable: false\n  }\n}]);</script></head><body class=\"align-middle text-xs-center\"><nav class=\"navbar\"><a class=\"navbar-brand\" href=\"/\"><img class=\"align-top\" src=\"/images/logo.png\" height=\"30\" alt=\"Startups Like Me\"></a></nav><div class=\"alert alert-danger\" id=\"globalError\" role=\"alert\" style=\"display:none\">Bad response. Please try again later.</div><style>.navbar {\n  background-color: transparent!important;\n  position: absolute;\n}\n</style><header class=\"text-white\"><div class=\"header-content\"><div class=\"header-content-inner\"><h1>Cool startups tailored for you !</h1><div class=\"line orange\"></div><div id=\"step1Email\" style=\"display:none\"><p>Simply vote on the startups you like<br> and we will give even better matches</p><form class=\"form-inline\" id=\"subscribe\"><div class=\"form-group\"><input class=\"form-control mt-1\" type=\"email\" name=\"email\" placeholder=\"Enter your email...\" required=\"\"><button class=\"btn btn-warning mt-1\" type=\"submit\">Subscribe</button><div class=\"form-control-feedback error\" style=\"display:none\"> Sorry, it appears that was an error. Please try again later.</div></div></form></div><div id=\"step1WebPush\"><p>Simply vote on the startups you like<br> and we will give even better matches</p><button class=\"btn btn-warning mt-1\" id=\"subscribeWebPush\">Subscribe</button><div class=\"form-control-feedback error\" style=\"display:none\"> It appears that was an error. Please try again later.</div></div><div id=\"step2\" style=\"display:none\"><div class=\"card card-inverse card-dark mx-auto text-xs-left\"><div class=\"card-block\"><div clss=\"card-text\"><form id=\"auth\"><fieldset class=\"form-group\"><legend>Web push or email?</legend><div class=\"form-check-inline\"><label class=\"form-check-label pl-0\"><input class=\"form-check-input\" id=\"daily\" type=\"radio\" name=\"notification_method\" value=\"web_push\" checked=\"checked\"> Web Push</label></div><div class=\"form-check-inline\"><label class=\"form-check-label\"><input class=\"form-check-input\" id=\"weekly\" type=\"radio\" name=\"notification_method\" value=\"email\"> Email</label></div></fieldset><fieldset class=\"form-group\"><legend>Email</legend><input class=\"form-control mt-1\" type=\"email\" name=\"email\" placeholder=\"Enter your email...\" required=\"\"></fieldset><fieldset class=\"form-group\"><legend>How often do you want to discover new startups?</legend><div class=\"form-check-inline\"><label class=\"form-check-label pl-0\"><input class=\"form-check-input\" id=\"daily\" type=\"radio\" name=\"frequency\" value=\"daily\"> Daily</label></div><div class=\"form-check-inline\"><label class=\"form-check-label\"><input class=\"form-check-input\" id=\"weekly\" type=\"radio\" name=\"frequency\" value=\"weekly\" checked=\"\"> Weekly</label></div><div class=\"form-check-inline\"><label class=\"form-check-label\"><input class=\"form-check-input\" id=\"monthly\" type=\"radio\" name=\"frequency\" value=\"monthly\"> Monthly</label></div></fieldset><div class=\"form-control-feedback error\" style=\"display:none\"> Sorry, it appears that was an error. Please try again later.</div><div class=\"text-xs-right\"><button class=\"btn btn-info\" type=\"submit\">Save</button></div></form></div></div></div></div><div id=\"done\" style=\"display:none\"><div class=\"card card-inverse card-success mx-auto text-xs-center\"><div class=\"card-block\"><blockquote class=\"card-blockquote\"> Thanks for signing up! Hope you enjoy!</blockquote></div></div></div></div></div></header><div id=\"about\"><section class=\"about container\"><h2 class=\"mb-3\">How it works</h2><div class=\"row\"><div class=\"col-md-4\"><span class=\"fa fa-building fa-5x\"></span><h3>We find the latest startups</h3><p>Our army of spiders crawls all through the web finding the latest and greatest startups.</p></div><div class=\"col-md-4\"><span class=\"fa fa-cogs fa-5x\"></span><h3>Machine Learning is applied</h3><p>We take your startup ratings to map you with better startups</p></div><div class=\"col-md-4\"><span class=\"fa fa-check fa-5x\"></span><h3>We email you the best</h3><p>We email the startups that best match you</p></div></div></section><section class=\"text-white night-mouse\"><h2>What we are about</h2><div class=\"line\"></div><p>Taking the work out of finding the perfect startup by doing the research for you</p></section></div><div class=\"team text-white\"><section class=\"grid\" id=\"team\"><div class=\"container\"><h2 class=\"mb-3\">Our Team</h2><div class=\"row text-md-left\"><div class=\"col-md-6 mb-2\"><img class=\"profile-picture float-md-left rounded\" src=\"images/robot.png\" alt=\"Tic-Toc\"><h3>Tic-Toc</h3><p>Tic-Toc works round the clock to bring you the best startups</p></div><div class=\"col-md-6 mb-2\"><img class=\"profile-picture float-md-left rounded\" src=\"images/stewart-park.png\" alt=\"Stewart Park\"><h3>Stewart Park</h3><p>It is unclear if he is just a really good programer or a AI from the future. All we know is he`s fueled by PBR</p></div></div><div class=\"row text-md-left\"><div class=\"col-md-6 mb-2\"><img class=\"profile-picture float-md-left rounded\" src=\"images/tim-best.png\" alt=\"Tim Best\"><h3>Tim Best</h3><p>Something something pun about his last name being best</p></div><div class=\"col-md-6 mb-2\"><img class=\"profile-picture float-md-left rounded\" src=\"images/uncle-sam.png\" alt=\"You\"><h3>You</h3><p>&#128521;</p></div></div></div></section></div><script type=\"text/javascript\">(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){\n(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),\nm=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)\n})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');\n\nga('create', 'UA-88453796-1', 'auto');\nga('send', 'pageview');</script><script src=\"//code.jquery.com/jquery-3.1.1.min.js\" integrity=\"sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=\" crossorigin=\"anonymous\"></script><script src=\"/app.min.js\"></script><script type=\"text/javascript\">$(function() {\n  /* Utils */\n  $.urlParam = function(name){\n    var results = new RegExp('[\\\\?&]' + name + '=([^&#]*)').exec(window.location.href);\n    return results[1] || 0;\n  };\n});</script><script type=\"text/javascript\">$(function() {\n  var frequency = 'daily';\n  var candidateID = null;\n  var access_token = null;\n  var email = null;\n  var onesignal_id = null;\n  var notification_method = null;\n\n  // ask for email if browser does not support web push\n  OneSignal.push(function() {\n    var isPushSupported = OneSignal.isPushNotificationsSupported();\n    if (isPushSupported) {\n      OneSignal.isPushNotificationsEnabled(function(isEnabled) {\n        $('#step1Email').hide();\n        if (isEnabled) {\n          $('#step1WebPush').hide();\n          $('#step2').show();\n        } else {\n          $('#step1WebPush').show();\n        }\n      });\n    } else {\n      $('#step1Email').show();\n      $('#step1WebPush').hide();\n    }\n  });\n\n  // Prompt user for push notification\n  $('#subscribeWebPush').click(function(event) {\n    OneSignal.push(function() {\n      OneSignal.registerForPushNotifications();\n    });\n  });\n\n  OneSignal.push(function() {\n  // Occurs when the user's subscription changes to a new value.\n  OneSignal.on('subscriptionChange', function (isSubscribed) {\n    if (isSubscribed) {\n      OneSignal.getUserId(function(userId) {\n        onesignal_id = userId;\n        notification_method = 'web_push';\n        var data = {\n          onesignal_id: onesignal_id,\n          notification_method: notification_method\n        };\n        function webpushSuccess(response) {\n          candidateID = response.id;\n          access_token = response.access_token;\n\n          $('input:radio[value=web_push]').click()\n\n          $('#step1WebPush').hide();\n          $('#step2').show();\n          $('#done').hide();\n        }\n        function webpushError(e) {\n          $('#step1WebPush .form-group').addClass('has-danger');\n          $('#step1Email .error').show();\n        }\n        postCandidates(data, webpushSuccess, webpushError);\n      });\n    }\n  });\n});\n\n  // submit email to create candidate\n  $('#step1Email #subscribe').submit(function(event) {\n    event.preventDefault();\n    email = $(\"#step1Email input[name='email']\").val()\n    notification_method = 'email'\n    var data = {\n      email: email,\n      notification_method: notification_method\n    };\n    function step1Success(response) {\n      candidateID = response.id;\n      access_token = response.access_token;\n\n      $('input:radio[value=email]').click()\n\n      $('#step1Email').hide();\n      $('#step2').show();\n      $(\"#step2 input[name='email']\").val(email);\n      $('#done').hide();\n    }\n    function step1error(e) {\n      $('#step1Email .form-group').addClass('has-danger');\n      if (e.error) {\n        $('#step1Email .error').html(e.error);\n      }\n      if (e.error.email) {\n        $('#step1Email .error').html(e.error.email);\n      }\n      $('#step1Email .error').show();\n    }\n    postCandidates(data, step1Success, step1error);\n  });\n\n  // Step 2 update preferances\n  $(\"#step2 input:radio[name=frequency]\").click(function() {\n    frequency = $(this).val();\n  });\n  $(\"#step2 input:radio[name=notification_method]\").click(function() {\n    notification_method = $(this).val();\n  });\n\n  $(\"#step2 #auth\").submit(function(event) {\n    event.preventDefault();\n    var data = {\n      access_token: access_token,\n      email: email,\n      frequency: frequency,\n      notification_method: notification_method\n    };\n    function step2Success(msg) {\n      $('#step1').hide();\n      $('#step2').hide();\n      $('#done').show();\n    }\n    function step2Error(e) {\n      $('#step2 .form-group').addClass('has-danger');\n      $('#step2 .error').show();\n    }\n    patchCandidates(candidateID, data, step2Success, step2Error);\n  });\n});</script></body></html>";

/***/ },
/* 5 */
/***/ function(module, exports) {

	module.exports = "<!DOCTYPE html><html lang=\"en\"><head><meta charset=\"utf-8\"><meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\"><meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\"><meta name=\"description\" content=\"Get alerts for the best startups for you.\"><meta name=\"robots\" content=\"noindex nofollow\"><title>Rate these startups | StartupsLikeMe</title><link rel=\"icon\" href=\"/images/favicon.png\" type=\"image/png\"><link rel=\"stylesheet\" href=\"//maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/css/bootstrap.min.css\" integrity=\"sha384-AysaV+vQoT3kOAXZkl02PThvDr8HYKPZhNT5h/CXfBThSRXQ6jW5DO2ekP5ViFdi\" crossorigin=\"anonymous\"><link href=\"//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css\" rel=\"stylesheet\" integrity=\"sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN\" crossorigin=\"anonymous\"><link rel=\"stylesheet\" href=\"/css/style.css\"><script src=\"https://cdn.onesignal.com/sdks/OneSignalSDK.js\" async=\"async\"></script><script type=\"text/javascript\">// development\n/*var OneSignal = window.OneSignal || [];\nOneSignal.push([\"init\", {\n  appId: \"9b2c1b78-a0d9-4760-92b3-3ef3b7589730\",\n  autoRegister: false,\n  subdomainName: 'startupslikemelocal',\n  safari_web_id: 'web.onesignal.auto.3c6dd35c-6a89-4a57-8823-48d881ba6e9f',\n  httpPermissionRequest: {\n    enable: true\n  },\n  notifyButton: {\n      enable: false\n  }\n}]);*/\n// production\nvar OneSignal = window.OneSignal || [];\nOneSignal.push([\"init\", {\n  appId: \"f8243a89-5790-400f-ab90-0cc7fbbedef6\",\n  autoRegister: false,\n  subdomainName: 'startupslikeme',\n  safari_web_id: 'web.onesignal.auto.235723f6-f2ef-49f2-bb5e-e966332d4e54',\n  httpPermissionRequest: {\n    enable: true\n  },\n  notifyButton: {\n      enable: false\n  }\n}]);</script></head><body class=\"align-middle text-xs-center\"><nav class=\"navbar\"><a class=\"navbar-brand\" href=\"/\"><img class=\"align-top\" src=\"/images/logo.png\" height=\"30\" alt=\"Startups Like Me\"></a></nav><div class=\"alert alert-danger\" id=\"globalError\" role=\"alert\" style=\"display:none\">Bad response. Please try again later.</div><div class=\"container text-xs-left mt-2\"><div id=\"startupsData\"></div></div><script type=\"text/javascript\">(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){\n(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),\nm=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)\n})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');\n\nga('create', 'UA-88453796-1', 'auto');\nga('send', 'pageview');</script><script src=\"//code.jquery.com/jquery-3.1.1.min.js\" integrity=\"sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=\" crossorigin=\"anonymous\"></script><script src=\"/app.min.js\"></script><script type=\"text/javascript\">$(function() {\n  /* Utils */\n  $.urlParam = function(name){\n    var results = new RegExp('[\\\\?&]' + name + '=([^&#]*)').exec(window.location.href);\n    return results[1] || 0;\n  };\n});</script><script type=\"text/javascript\">$(function() {\n  var emailId = $.urlParam('email_id');\n  var access_token = $.urlParam('access_token');\n\n  function drawTable(data) {\n    for (var i = 0; i < data.length; i++) {\n      drawRow(data[i]);\n    }\n    $('.intrest').click(function(event) {\n      var currentIntrest = $(this);\n      var startupID = $(this).data('startup-id');\n      var ratingID = $(this).data('rating-id');\n      var intrestLevel = $(this).data('intrest');\n      if($(this).hasClass('active')) {\n        intrestLevel = 0;\n      }\n      if (ratingID) {\n        // patch rating\n        var ratingData = {\n          access_token: access_token,\n          intrest_level: intrestLevel\n        };\n        function successFn(msg) {\n          if(intrestLevel > 0.5) {\n            currentIntrest.addClass('active btn-success');\n          } else {\n            currentIntrest.removeClass('active btn-success');\n          }\n        }\n        function errorFn(msg) {\n          $('#globalError').show();\n        }\n        patchRatings(ratingID, ratingData, successFn, errorFn);\n      } else {\n        // create rating\n        var ratingData = {\n          access_token: access_token,\n          email_id: emailId,\n          startup_id: startupID,\n          intrest_level: intrestLevel\n        }\n        function successFn(msg) {\n          currentIntrest.data('rating-id', msg.id);\n          if(intrestLevel > 0.5) {\n            currentIntrest.addClass('active btn-success');\n          } else {\n            currentIntrest.removeClass('active btn-success');\n          }\n        }\n        function errorFn(msg) {\n          $('#globalError').show();\n        }\n        postRatings(ratingData, successFn, errorFn);\n\n      }\n    });\n  }\n\n  function drawRow(rowData) {\n    var locations = rowData.locations.map(function(l){return l.name;});\n    var industries = rowData.industries.map(function(l){return l.name;});\n    var startupsData = [\n      '<div class=\"card\" style=\"max-width: none\">',\n        '<div class=\"card-header\">',\n          '<h3 class=\"mb-0\">',\n            '<img src=\"',rowData.image_url,'\" alt=\"',rowData.name,'\" height=\"50\" class=\"mr-1\">',\n            rowData.name,\n            '<button class=\"intrest btn btn-secondary pull-right ml-1\" id=\"rateStartup',rowData.id,'\" data-startup-id=\"', rowData.id, '\" data-intrest=\"0\"><i class=\"fa fa-thumbs-down\"></i> Dislike</button>',\n            '<button class=\"intrest btn btn-secondary pull-right\" id=\"rateStartup',rowData.id,'\" data-startup-id=\"', rowData.id, '\" data-intrest=\"1\"><i class=\"fa fa-thumbs-up\"></i> Like</button>',\n\n          '</h3>',\n        '</div>',\n        '<div class=\"card-block\">',\n          '<div class=\"card-text\"><b>Description: </b>',rowData.description,'</div>',\n    ];\n    if (locations.length) {\n      startupsData.push('<div class=\"card-text\"><b>Locations: </b>',locations.join(', '),'</div>');\n    }\n    if (industries.length) {\n      startupsData.push('<div class=\"card-text\"><b>Industry: </b>',industries.join(', '),'</div>');\n    }\n    if (rowData.number_of_employees) {\n      startupsData.push('<div class=\"card-text\"><b>Employees: </b>~',rowData.number_of_employees,'</div>');\n    }\n    startupsData.push(\n      '<div class=\"card-text\"><b>Website: </b><a href=\"',rowData.website_url,'\" class=\"card-link\" target=\"_blank\">',rowData.website_url,'</a></div>',\n        '<div class=\"card-text\"><b>links: </b>'\n    );\n    if (rowData.crunchbase_url) {\n      startupsData.push('<a href=\"',rowData.crunchbase_url,'\" target=\"_blank\" class=\"mr-1\">crunchbase.com</a>');\n    }\n    if (rowData.angellist_url) {\n      startupsData.push('<a href=\"',rowData.angellist_url,'\" target=\"_blank\" class=\"mr-1\">angel.co</a>');\n    }\n    startupsData.push('</div></div></div>');\n\n    $('#startupsData').append(startupsData.join(''));\n  }\n\n  /* API calls */\n\n  // get startups\n  var data = {\n    email_id: emailId,\n    access_token: access_token\n  };\n  function errorFn(msg) {\n    $('#globalError').show();\n  }\n  listStartups(data, drawTable, errorFn);\n\n  // get ratings\n  function successFn(ratings) {\n    for(i in ratings) {\n      var likeBtn = $('#rateStartup'+ratings[i].startup_id);\n      likeBtn.data('rating-id', ratings[i].id);\n      if(ratings[i].intrest_level  > 0.5) {\n        likeBtn.addClass('active btn-success');\n      }\n    }\n  }\n  listRatings(data, successFn, errorFn);\n\n});</script></body></html>";

/***/ },
/* 6 */
/***/ function(module, exports) {

	module.exports = "<!DOCTYPE html><html lang=\"en\"><head><meta charset=\"utf-8\"><meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\"><meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\"><meta name=\"description\" content=\"Get alerts for the best startups for you.\"><meta name=\"robots\" content=\"noindex nofollow\"><title>Update Frequency | StartupsLikeMe</title><link rel=\"icon\" href=\"/images/favicon.png\" type=\"image/png\"><link rel=\"stylesheet\" href=\"//maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/css/bootstrap.min.css\" integrity=\"sha384-AysaV+vQoT3kOAXZkl02PThvDr8HYKPZhNT5h/CXfBThSRXQ6jW5DO2ekP5ViFdi\" crossorigin=\"anonymous\"><link href=\"//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css\" rel=\"stylesheet\" integrity=\"sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN\" crossorigin=\"anonymous\"><link rel=\"stylesheet\" href=\"/css/style.css\"><script src=\"https://cdn.onesignal.com/sdks/OneSignalSDK.js\" async=\"async\"></script><script type=\"text/javascript\">// development\n/*var OneSignal = window.OneSignal || [];\nOneSignal.push([\"init\", {\n  appId: \"9b2c1b78-a0d9-4760-92b3-3ef3b7589730\",\n  autoRegister: false,\n  subdomainName: 'startupslikemelocal',\n  safari_web_id: 'web.onesignal.auto.3c6dd35c-6a89-4a57-8823-48d881ba6e9f',\n  httpPermissionRequest: {\n    enable: true\n  },\n  notifyButton: {\n      enable: false\n  }\n}]);*/\n// production\nvar OneSignal = window.OneSignal || [];\nOneSignal.push([\"init\", {\n  appId: \"f8243a89-5790-400f-ab90-0cc7fbbedef6\",\n  autoRegister: false,\n  subdomainName: 'startupslikeme',\n  safari_web_id: 'web.onesignal.auto.235723f6-f2ef-49f2-bb5e-e966332d4e54',\n  httpPermissionRequest: {\n    enable: true\n  },\n  notifyButton: {\n      enable: false\n  }\n}]);</script></head><body class=\"align-middle text-xs-center\"><nav class=\"navbar\"><a class=\"navbar-brand\" href=\"/\"><img class=\"align-top\" src=\"/images/logo.png\" height=\"30\" alt=\"Startups Like Me\"></a></nav><div class=\"alert alert-danger\" id=\"globalError\" role=\"alert\" style=\"display:none\">Bad response. Please try again later.</div><section class=\"container\"><h1 class=\"my-2\">Update Frequency</h1><div class=\"alert alert-success\" id=\"done\" role=\"alert\" style=\"display:none\"> Your email frequency has been updated!</div><form id=\"unsubscribe\"><fieldset class=\"form-group\"><legend>Frequency?</legend><div class=\"form-check-inline\"><label class=\"form-check-label pl-0\"><input class=\"form-check-input\" id=\"never\" type=\"radio\" name=\"frequency\" value=\"never\"> Never</label></div><div class=\"form-check-inline\"><label class=\"form-check-label pl-0\"><input class=\"form-check-input\" id=\"daily\" type=\"radio\" name=\"frequency\" value=\"daily\"> Daily</label></div><div class=\"form-check-inline\"><label class=\"form-check-label\"><input class=\"form-check-input\" id=\"weekly\" type=\"radio\" name=\"frequency\" value=\"weekly\" checked=\"\"> Weekly</label></div><div class=\"form-check-inline\"><label class=\"form-check-label\"><input class=\"form-check-input\" id=\"monthly\" type=\"radio\" name=\"frequency\" value=\"monthly\"> Monthly</label></div><div class=\"form-control-feedback error\" style=\"display:none\"> Sorry, it appears that was an error. Please try again later?</div></fieldset><div class=\"text-xs-right\"><button class=\"btn btn-info\" type=\"submit\">Save</button></div></form></section><script type=\"text/javascript\">(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){\n(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),\nm=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)\n})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');\n\nga('create', 'UA-88453796-1', 'auto');\nga('send', 'pageview');</script><script src=\"//code.jquery.com/jquery-3.1.1.min.js\" integrity=\"sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=\" crossorigin=\"anonymous\"></script><script src=\"/app.min.js\"></script><script type=\"text/javascript\">$(function() {\n  /* Utils */\n  $.urlParam = function(name){\n    var results = new RegExp('[\\\\?&]' + name + '=([^&#]*)').exec(window.location.href);\n    return results[1] || 0;\n  };\n});</script><script type=\"text/javascript\">$(function() {\n  var frequency = \"daily\";\n  var canidateId = $.urlParam('candidate');\n  var access_token = $.urlParam('access_token');\n\n  $(\"#unsubscribe input:radio[name=frequency]\").click(function() {\n    frequency = $(this).val();\n  });\n\n  $(\"#unsubscribe\").submit(function(event) {\n    event.preventDefault();\n    var data = {\n      frequency: frequency,\n      access_token: access_token\n    };\n    function unsubSuccess(msg) {\n      $('#done').show();\n    }\n    function unsubError(msg) {\n      $('#unsubscribe .error').show();\n    }\n    patchCandidates(canidateId, data, unsubSuccess, unsubError);\n  });\n});</script></body></html>";

/***/ }
/******/ ])
});
;