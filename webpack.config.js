const CopyWebpackPlugin = require('copy-webpack-plugin');
const StaticSiteGeneratorPlugin = require('static-site-generator-webpack-plugin');
const webpack = require("webpack");
const path = require("path");

const paths = [
  '404.html',
  '/',
  '/email-frequency/',
  '/rate/',
];

const websiteConfig = {
  entry: ['whatwg-fetch', './src/routes.js'],

  output: {
    path: path.resolve(__dirname + '/dist'),
    filename: 'main.js',
    /* IMPORTANT!
     * You must compile to UMD or CommonJS
     * so it can be required in a Node context: */
    libraryTarget: 'umd'
  },

  module: {
    loaders: [{
      include: /\.pug/,
      // pass options to pug as a query ('pug-html-loader?pretty')
      loader: 'pug-html-loader'
    }]
  },

  devServer: {
    port: 3000,
  },

  plugins: [
    new StaticSiteGeneratorPlugin('main', paths),
    new CopyWebpackPlugin([
      { from: './src/static/' },
    ])
  ]
};

const jsConfig = {
  entry: ['whatwg-fetch', './src/app.js'],
  output: {
    path: path.resolve(__dirname + '/dist'),
    filename: 'app.min.js'
  },
  module: {
    loaders: [{
      include: /\.pug/,
      // pass options to pug as a query ('pug-html-loader?pretty')
      loader: 'pug-html-loader'
    }]
  },
  plugins: [
    new webpack.DefinePlugin({
      'NODE_ENV': JSON.stringify(process.env.NODE_ENV || 'development')
    })
  ]
};

module.exports = [websiteConfig, jsConfig];
