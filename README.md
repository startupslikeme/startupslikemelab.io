# StartupsLike.me
[![build status](https://gitlab.com/startupslikeme/startupslikeme.gitlab.io/badges/master/build.svg)](https://gitlab.com/startupslikeme/startupslikeme.gitlab.io/commits/master)
[![coverage report](https://gitlab.com/startupslikeme/startupslikeme.gitlab.io/badges/master/coverage.svg)](https://gitlab.com/startupslikeme/startupslikeme.gitlab.io/commits/master)

## Setup
```bash
npm install
npm install -g webpack
```

## Build
```bash
npm run build
```

## Run Locally
```bash
npm run start
```

## Testing
```bash
npm test
```
